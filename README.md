# How to run it?

Create a virtual environment in cloned folder

```
$ python3 -m venv .
```

Activate it:
```
$ source bin/activate
(flaskapp-base) $
```

Install requirements:
```
$ pip3 install -r requirements.txt
```

Once requirements are installed, run the app:
```
$ python3 app.py
```

